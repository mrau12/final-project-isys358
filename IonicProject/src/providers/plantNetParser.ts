﻿
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import * as cheerio from 'cheerio';
import * as request from 'request';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class plantNetParser {
    constructor(private http: Http) { }
    Parser_(url: string) {
        return new Promise(function (resolve, reject) {
            request(url, function (error, response, body) {
                if (error) {
                    reject(error);
                }
                else {
                    const dsc = [];
                    let $ = cheerio.load(body);
                    let temp = $("body").children('table').eq(1).children('tr').children('td').eq(1).children('table').children('tr').children('td').find('p');
                    temp.each(function (i, elem) {
                        const p = $(elem);
                        if (p.attr('clear') === 'all') {
                            return false;
                        }
                        dsc.push(p);
                    });
                    resolve(dsc);
                }
            });
        });
    }
    Parser(url: string) {
        return this.selfrequest(url).then((body) => {
            const dsc = [];
            let $ = cheerio.load(body);
            let temp = $("body").children('table').eq(1).children('tr').children('td').eq(1).children('table').children('tr').children('td').find('p');
            temp.each(function (i, elem) {
                const p = $(elem);
                if (p.attr('clear') === 'all') {
                } else{
                    console.log("each elem is :",p.html());
                    dsc.push(p);
                }
            });
            return dsc;
        });
    }
    selfrequest(url: string): Promise<string> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(url)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.text();
        return body || '';
    }
    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.text() || '';
            const err = body
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Promise.reject(errMsg);
    }
}
