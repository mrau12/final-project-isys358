﻿//package for processing json file
declare module '*.json';
import data from "../assets/data/plant_nested_data.json";
import CCdsc from "../assets/data/cc_description_all.json";
import groups from "../assets/data/groups.json";
import topfamily from "../assets/data/top_family.json";
import commonName from "../assets/data/common_name.json"
// creating a service provider for loading json file
export class jsonLoader {
    constructor() { }
    getData() {
        return data;
    }
    getCCdsc(){
        return CCdsc;
    }
    getGroups() {    	
        return groups;
    }
    getTopFaimily(){
        return topfamily;
    }
    getCommonName(){
        return commonName;
    }
}