﻿import { Injectable } from '@angular/core';
@Injectable()
export class ShareService {
    //Global variables plant index
    shareJSONIndex: any;

    //Global variable for whole genus list formatted with {'genus_name':['family_name']}
    shareGenusList: any;

    //Global variable for whole species list formatted with {'genus_name species_name':['family-name','genus_name','species_name']}
    shareSpList: any;

    //Global variable for whole subspecies list formatted with {'genus_name species_name subspecies_name':['family-name','genus_name','species_name','species_name']}
    sharesubspList: any;
    //Global variable for current description
    currentDSC: string = "default message.";
    
    //Global variable for plant identfication keys
	shareGroups: any;
    //Global variable for Key to class index
    keyToClass: any;
    // Global variable for central coast plant description
    ccDsc:{} = {};
    // Global variable to check if there is a current report.
    currentReportExists = false;
    // Global variable for current report title.
    currentReportTitle;
    // Global variable for top family index.
    topIndex: any;
    // Global variable for common name list
    common_name_index : any;
    constructor() {

    }
    setDSC(dsc: string) {
        this.currentDSC = dsc;
    }
    getDSC() {
        return this.currentDSC;
    }

    setupJSONIndex(file: JSON) {
        this.shareJSONIndex = file;
    }
    getJsonIndex() {
        return this.shareJSONIndex;
    }
    setupGenusLis(file: any) {
        this.shareGenusList = file;
    }
    getGenusList() {
        return this.shareGenusList;
    }
    setupSplist(file: any) {
        this.shareSpList = file;
    }
    getSplist() {
        return this.shareSpList;
    }

    setupSubspList(file: any) {
        this.sharesubspList = file;
    }

    getSubspList() {
        return this.sharesubspList;
    }
    getCurrentReportExists() {
        return this.currentReportExists;
    }

    setCurrentReportExists(boo) {
        this.currentReportExists = boo;
    }

    getCurrentReportTitle() {
        return this.currentReportTitle;
    }

    setCurrentReportTitle(title) {
        this.currentReportTitle = title;
    }
    setKeyToClass(value) {
        this.keyToClass = value;
    }
    getKeyToClass() {
        return this.keyToClass;
    }
    setCCdsc(value){
        this.ccDsc = value;
    }
    getCCdsc(){
        return this.ccDsc;
    }
    getCClIdex(){
        return Object.keys(this.ccDsc).concat(Object.keys(this.common_name_index));
    }
    setGroups(value){
		this.shareGroups = value;
	}
	getGroups(){
		return this.shareGroups;
	}
    setTopFamily(value){
        this.topIndex=value;
    }
    getTopFamily(){
        return this.topIndex;
    }
    setCommonNameIndex(value){
        this.common_name_index = value;
    }
    getCommonNameIndex(){
        return this.common_name_index;
    }
}