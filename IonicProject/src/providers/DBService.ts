
import { PlantItem } from './../pages/plantItemPage/plantItem';
import { Report } from './../pages/reportPage/report';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { plantInformation } from "../pages/itemDetailPage/PlantInformation";

@Injectable()
/**
 * Storage: top level is like {'reportInfo':{"currentReport":false/true,"reportID":0},'reportid':{'id':id,'t,..},}
 *          top level pl'+reportID:[IDcounter,plantItem,..,..]
 * 			          plantItem is going to be pid:{'pid':pid,'pl':pl,'flag':flag,'comments':comments},counter:0
 *          top level {'session':'search'|'report'}
 *  global counter for report ID and global currentReport flag 'reportInfo':{"currentReport":false,"reportID":0}
 *  'currentReport':{},'trashReport1':t1,'trashReport2':t2
 *  'currentReport' is used to store temporay current report
 *  'trashReport1','trashReport2' are used to store last two deleted reports, in case users want to restore report
 *  'reportid':{}
 * 	report structure would be like 
 * 			'reportid':{'id':id,'title':title, 'author':author, 'date':date,'location':loca,'comments':comments}
 *          '
 */
export class DBservice {
    public session: string = 'search';
    public selectingReportID: number = 0;
    public currentReportFlag: Boolean;
    constructor(public storage: Storage, public events: Events) {
    }
    //----------------------------------- report functions -----------------------------------
    public getReportList(): Promise<Report[]> {
        console.log('DBservice current report flag: ', this.currentReportFlag);
        let templist: Report[] = [];
        return this.storage.forEach((value, key, number) => {
            let isNumber = !Number.isNaN(Number(key));
            if (isNumber) {
                templist.push(this.load(value));
            }
        }).then(() => {
            return templist.reverse(); //reverse order
        });
    }
    private load(value: any): Report {
        let tempReport = new Report();
        tempReport.$reportID = value.id;
        tempReport.$reportTitle = value.title;
        tempReport.$date = value.date;
        return tempReport;
    }

    public createNewReport() {
        return new Promise((resolve, reject) => {
            let report = new Report();
            report.$date = new Date().toISOString();
            this.getIDcounter().then((counter) => {
                if (this.currentReportFlag === true) {
                    reject();
                } else {
                    report.$reportID = counter + 1;
                    let str = 'New report ' + (report.$reportID).toString();
                    report.$reportTitle = str;
                    let parsedData = report.generateJSON();
                    this.storage.set('reportInfo', { "currentReport": true, "reportID": report.$reportID });
                    this.storage.set('currentReport', parsedData);
                    this.currentReportFlag = true;
                    resolve();
                }
            });
        });
    }
    /**
     * Store report to database
     * @param Report report that need to be stored
     * @param option option: 'current' | 'saved' | 'new'  \n 'current' is used to update current report, \n 'new' is used to create a new 'current' report,\n 'saved' is used to update existing report.
     */
    public saveReportToDBAsync(report: Report, option: string) {
        return new Promise((resolve, reject) => {
            if (option === 'current') {
                let parsedData = report.generateJSON();
                this.storage.set('currentReport', parsedData);
                resolve();
            }
            if (option === 'new') {
                this.getIDcounter().then((counter) => {
                    if (this.currentReportFlag === true) {
                        reject();
                    } else {
                        report.$reportID = counter + 1;
                        let str = 'New report ' + (report.$reportID).toString();
                        report.$reportTitle = str;
                        let parsedData = report.generateJSON();
                        this.storage.set('reportInfo', { "currentReport": true, "reportID": report.$reportID });
                        this.storage.set('currentReport', parsedData);
                        this.currentReportFlag = true;
                        resolve();
                    }
                });
            }
            if (option === 'saved') {
                this.getIDcounter().then((counter) => {
                    if (report.$reportID > counter || report.$reportID === 0 || report.$reportID === undefined) {
                        reject();
                    } else {
                        let tempid = report.$reportID.toString();
                        this.storage.set(tempid, report.generateJSON());
                        resolve();
                    }
                });
            }
            if (option === "test") {
                let tempid = report.$reportID.toString();
                this.storage.set('reportInfo', { "currentReport": this.currentReportFlag, "reportID": report.$reportID });
                let parsedData = report.generateJSON();
                this.storage.set(tempid, parsedData);
            }
        });
    }
    /*
    //todo
    public saveReportToDBwithLoading(report: Report) {
        this.storage.ready().then(() => {

        });
    }*/
    public getReportByID(reportID: number): Promise<Report> {
        return new Promise((resolve, reject) => {
            let num = reportID.toString()
            this.storage.get(num).then((rawData) => {
                if (rawData === undefined) {
                    reject();
                }
                else {
                    let result = new Report();
                    result.loadData(rawData);
                    resolve(result);
                }
            });
        });
    }

    //todo
    public removeReport(reportID: number) {
        return new Promise((resolve, reject) => {
            let plid = 'pl'.concat(reportID.toString());
            this.storage.remove(plid).then(() => {
                this.getCurrentReport().then((report) => {
                    if (reportID === report.$reportID && this.currentReportFlag === true) {
                        this.storage.set('currentReport', {}).then(() => {
                            this.setCurrentReportFlag(false).then(() => {
                                this.currentReportFlag = false;
                            });
                        });
                    }
                }).then(() => {
                    this.storage.remove(reportID.toString()).then(() => {
                        resolve();
                    });
                });
            });
        });
    }
    //todo
    //----------------------------------- plantItem functions -----------------------------------
    //todo

    //it does not handle any plantItem ID here

    //todo
    public getPlantItem(pid: number, reportID: number): Promise<PlantItem> {
        return new Promise((resolve, reject) => {
            let plid = 'pl'.concat(reportID.toString());
            this.storage.get(plid).then((plantitemlist) => {
                if (plantitemlist === null || plantitemlist === undefined) {
                    reject();
                }
                else {
                    let pl = new PlantItem();
                    console.log('load item', plantitemlist);
                    pl.loadData(plantitemlist[pid.toString()]);
                    resolve(pl);
                }
            });
        });
    }
    //todo
    public removePlantItem(pid: number, reportID: number) {
        return new Promise((resolve, reject) => {
            let plid = 'pl'.concat(reportID.toString());
            this.storage.get(plid).then((plantitemlist) => {
                if (plantitemlist === null || plantitemlist === undefined) {
                    reject();
                }
                else {

                    delete plantitemlist[pid.toString()];
                    this.storage.set(plid, plantitemlist);
                    resolve();
                }
            });
        });
    }
    //----------------------------------- current plantItem functions -----------------------------------

    public getCurrentReport(): Promise<Report> {
        return this.storage.get('currentReport').then((raw) => {
            let report = new Report();
            report.loadData(raw);
            return report;
        });
    }
    public updateCurrentReport(report: Report) {
        return new Promise((resolve, reject) => {
            if (this.currentReportFlag === false) {
                reject();
            }
            else {
                this.storage.set('currentReport', report.generateJSON());
                resolve();
            }

        });
    }
    public CurrentTobeSaved() {
        return new Promise((resolve, reject) => {
            if (this.currentReportFlag === false) {
                reject();
            } else {
                this.getCurrentReport().then((report) => {
                    let tempid = report.$reportID.toString();
                    this.storage.set('reportInfo', { "currentReport": false, "reportID": report.$reportID });
                    this.storage.set(tempid, report.generateJSON()).then(() => {
                        this.storage.set('currentReport', {});
                    });
                    this.currentReportFlag = false;
                    resolve();
                });
            }
        });
    }
    // ----------------------------------- flags and id counter -----------------------------------

    public getIDcounter(): Promise<number> {
        return this.storage.get('reportInfo').then((val) => {
            return val.reportID;
        });
    }

    /**
     * return a promise with current flag
     */
    public getCurrentReportFlag(): Promise<boolean> {
        return this.storage.get('reportInfo').then((val) => {
            this.currentReportFlag = val.currentReport;
            console.log('get triggered!');
            return val.currentReport;
        });
    }
    /**
     * 
     * @param flag 
     */
    public setCurrentReportFlag(flag: boolean) {
        return this.storage.get('reportInfo').then((val) => {
            this.currentReportFlag = flag;
            return this.storage.set('reportInfo', { "currentReport": flag, "reportID": val.reportID });
        });

    }


    // only replace existing plantItem
    //-------------------------------------------
    public savePlantItemToDB(plitem: PlantItem, reportID: number) {
        return new Promise((resolve, reject) => {
            let plid = 'pl'.concat(reportID.toString());
            let data = plitem.generateJson();
            return this.storage.get(plid).then((plantitemlist) => {
                if (plantitemlist === null || plantitemlist === undefined) {
                    reject();
                }
                else {
                    let pid = plitem.pid;
                    plantitemlist[pid.toString()] = data;
                    this.storage.set(plid, plantitemlist);
                    resolve();
                }
            });
        });
    }

    public savePlantItemListToDB(plantList: PlantItem[], reportID: number) {
        return new Promise((resolve, reject) => {
            let plid = 'pl'.concat(reportID.toString());
            this.storage.get(plid).then((plantitemlist) => {
                if (plantitemlist === null || plantitemlist === undefined) {
                    reject();
                }
                else {
                    for (let item of plantList) {
                        let data = item.generateJson();
                        let pid = item.pid;
                        plantitemlist[pid.toString()] = data;
                    }
                    console.log("saved plantList", plantitemlist);
                    this.storage.set(plid, plantitemlist).then(() => {
                        resolve();
                    });

                }
            });
        });
    }
    private getPlantItemListJSON(reportID: number): Promise<any> {
        return new Promise((resolve, reject) => {
            let plid = 'pl'.concat(reportID.toString());
            this.storage.get(plid).then((plantitemlist) => {
                if (plantitemlist === null || plantitemlist === undefined || plantitemlist === {}) {
                    resolve({ 'counter': 0 });
                }
                else {
                    resolve(plantitemlist); //{'pid':{..},'counter':0}
                }
            });
        });
    }
    public getPlantItemList(reportID: number): Promise<{}[]> {
        return new Promise((resolve, reject) => {
            this.getPlantItemListJSON(reportID).then((json) => {
                let templist = [];
                for (let key in json) {
                    let isNumber = !Number.isNaN(Number(key));
                    if (isNumber) {
                        templist.push(json[key]);
                    }
                }
                resolve(templist.reverse());
            });
        });
    }
    public savePlantItemToCurrent(plinfo: plantInformation) {
        return new Promise((resolve, reject) => {
            let pl = new PlantItem();
            pl.$pl = plinfo;
            if (this.currentReportFlag === false) {
                reject();
            } else {
                this.getCurrentReport().then((report) => {
                    this.getPlantItemListJSON(report.$reportID).then((plist) => { //{'counter':0}
                        let plid = 'pl'.concat(report.$reportID.toString());
                        let pid = plist.counter + 1;
                        pl.pid = pid;
                        plist[pid.toString()] = pl.generateJson();
                        plist['counter'] = plist.counter + 1;
                        console.log('generating', plist);
                        this.storage.set(plid, plist).then(() => {
                            resolve();
                        });
                    });
                });
            }

        });
    }
    public savelantItemToSelected(plinfo: plantInformation, reportID: number) {
        return new Promise((resolve, reject) => {
            let pl = new PlantItem();
            pl.$pl = plinfo;
            this.getPlantItemListJSON(reportID).then((plist) => { //{'counter':0}
                let plid = 'pl'.concat(reportID.toString());
                let pid = plist.counter + 1;
                pl.pid = pid;
                plist[pid.toString()] = pl.generateJson();
                plist['counter'] = plist.counter + 1;
                console.log('generating', plist);
                this.storage.set(plid, plist).then(() => {
                    resolve();
                });
            });
        });
    }
    //----------------------------------- listener ---------------------------------


    public updateSession(str: string) {
        this.session = str;
        return this.storage.set('session', str);
    }
    public getSession(): Promise<string> {
        return this.storage.get('session').then((sess) => {
            return sess;
        });
    }
    
    _getCurrentReportFlag() {
        //this.events.publish('getEvent');
        return this.currentReportFlag;
    }
    public setSelectingReportID(id:number){
        this.selectingReportID = id;
        return this.storage.set('selectingReportID', id);
    }
    // need others for trashes
}