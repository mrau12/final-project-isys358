﻿import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { IonicStorageModule } from '@ionic/storage';


import { ReportSationPage } from '../pages/reportSationPage/reportSationPage';
import { ReportPage } from '../pages/reportPage/reportPage';
import { About } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { PlantItemPage } from '../pages/plantItemPage/plantItemPage';
import { DetailItemPage } from '../pages/itemDetailPage/item';
import { KeyPage } from '../pages/key/key';

import { Autoresize } from '../pages/reportPage/autosize'
import { DscComponent } from '../pages/itemDetailPage/dsc.component';
import { PlantCard } from "../pages/plantCard/PlantCard.component";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';


import { jsonLoader } from '../providers/jsonLoader';
import { ShareService } from '../providers/shareService';
import { plantNetParser } from '../providers/plantNetParser';
import { DBservice } from "../providers/DBService";



@NgModule({
  declarations: [
    MyApp,
    ReportSationPage,
	ReportPage,
    HomePage,
    TabsPage,
	PlantItemPage,
  KeyPage,
    DscComponent,
    Autoresize,
    DetailItemPage,
	About,
  PlantCard
  ],
  imports: [
      BrowserModule,
      HttpModule,
      BrowserAnimationsModule,
    IonicModule.forRoot(MyApp,
    {platforms : {
          ios : {
            // These options are available in ionic-angular@2.0.0-beta.2 and up.
            scrollAssist: false,    // Valid options appear to be [true, false]
            autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
          }
        }
    }),
    IonicStorageModule.forRoot({
        name: '__reportdb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
      })   
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ReportSationPage,
	ReportPage,
    HomePage,
    TabsPage,
	PlantItemPage,
  KeyPage,
    DetailItemPage,
	About
  ],
  providers: [
    jsonLoader,
    StatusBar,
    SplashScreen,
    Keyboard,
    ShareService,
    DBservice,
    plantNetParser,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})

export class AppModule {}
