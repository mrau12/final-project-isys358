﻿import { DBservice } from './../providers/DBService';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Storage } from '@ionic/storage';
import { TabsPage } from '../pages/tabs/tabs';
import { jsonLoader } from '../providers/jsonLoader';
import { ShareService } from '../providers/shareService';
import { Keyboard } from '@ionic-native/keyboard';
import { AlertController,Events } from 'ionic-angular';

@Component({
    templateUrl: 'app.html',
})
export class MyApp {
    rootPage: any = TabsPage;
    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public shareService: ShareService, public jsonloader: jsonLoader, public keyboard: Keyboard, private storage: Storage,public dbservice: DBservice,public alert:AlertController,public events:Events) {
        //Set up the storage file to keep track of all the reports
        //this.storage.clear();
        this.storage.ready().then(() => {
           // this.storage.clear();
            this.storage.get('reportInfo').then((val) => {
                if (val == null) {
                    this.storage.set('reportInfo', { 'currentReport': false, 'reportID': 0 })
                    console.log('Initial Storage');
                    this.storage.get('report').then((val) => {
                        if (val == null) {
                            this.storage.set('currentReport', {});
                            this.storage.set('trashReport1', {});
                            this.storage.set('trashReport2', {});
                            this.storage.set('selectingReportID', 0);
                            this.storage.set('session', '');
                            console.log('Initial report');            
                        }
                    });
                } //testing
                else {
                    this.storage.get('reportInfo').then((val) => {
                        console.log('driver is', this.storage.driver);
                    });
                }
                this.platformReady();
            }).then(()=>{
                this.storage.get('reportInfo').then((val) => {
                    let temp = val.currentReport;
                    this.dbservice.currentReportFlag = temp;
                });              
            });
        });
        this.initialGlobalVariables();
        this.ListenToSession();
        console.log("current Platform:", this.platform.platforms());
    }
    initialGlobalVariables() {
        let commonNameIndex = JSON.parse(JSON.stringify(this.jsonloader.getCommonName()));
        this.shareService.setCommonNameIndex(commonNameIndex);
        let topindex = JSON.parse(JSON.stringify(this.jsonloader.getTopFaimily()));
        this.shareService.setTopFamily(topindex);
        var groupdata = JSON.parse(JSON.stringify(this.jsonloader.getGroups()));
        this.shareService.setGroups(groupdata);
        // set up a global variable foir central coast plant description
        let ccDsc = JSON.parse(JSON.stringify(this.jsonloader.getCCdsc()));
        this.shareService.setCCdsc(ccDsc);
        // set up a global variable for plant index
        var jsondata = JSON.parse(JSON.stringify(this.jsonloader.getData()));
        this.shareService.setupJSONIndex(jsondata);
        // set up a gloable variable for list of whole genus/species/subspecies in database
        var myjsondata = this.shareService.getJsonIndex();
        var familylist = Object.keys(myjsondata).sort();
        var templst_g = {};
        var templst_g_sp = {};
        var templst_subsp = {};
        for (let x = 0; x < familylist.length; x++) {
            var family_name = familylist[x];
            var genusjson = myjsondata[family_name]["genus"];
            var genus_list = Object.keys(genusjson);
            for (let i = 0; i < genus_list.length; i++) {
                var genus_name = genus_list[i];
                templst_g[genus_name] = [family_name];
                var spjson = myjsondata[family_name]["genus"][genus_name]["species"];
                var splist = Object.keys(spjson);
                for (let y = 0; y < splist.length; y++) {
                    var species_name = splist[y];
                    var gsp_name = genus_name.concat(" ", species_name);
                    templst_g_sp[gsp_name] = [family_name, genus_name, species_name];
                      spjson = myjsondata[family_name]["genus"][genus_name]["species"][species_name]["variants"];
                    if (Object.keys(spjson).length === 0 && spjson.constructor === Object) {

                    }
                    else {
                        var subsplist = Object.keys(spjson);
                        for (let e = 0; e < subsplist.length; e++) {
                            var subspecies_name = subsplist[e];
                            var subsp_full = gsp_name.concat(" ", subspecies_name);
                            templst_subsp[subsp_full] = [family_name, genus_name, species_name, subspecies_name];
                        }
                    }
                }
            }
        }
        this.shareService.setupGenusLis(templst_g);
        this.shareService.setupSplist(templst_g_sp);
        this.shareService.setupSubspList(templst_subsp);
    }
    platformReady() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            if (this.platform.is('ios')) {
                this.keyboard.disableScroll(true);
            }
        });
    }
   exit(){
      let alert = this.alert.create({
        title: 'Confirm',
        message: 'Do you want to exit?',
        buttons: [{
          text: "exit?",
          handler: () => { this.exitApp() }
        }, {
          text: "Cancel",
          role: 'cancel'
        }]
      })
      alert.present();
  }
  exitApp(){
    this.platform.exitApp();
  }
  ListenToSession(){
      this.events.subscribe('searchEntry', () => {
            this.dbservice.updateSession('search');
            console.log('Search tab enteted');
      });
       this.events.subscribe('reportEntry', () => {     
            this.dbservice.updateSession('report');
            console.log('report tab entered');
      });
      this.events.subscribe('selected:reportid',(id)=>{
            this.dbservice.setSelectingReportID(id);
      });
      //this.events.subscribe('search')
  }

}
