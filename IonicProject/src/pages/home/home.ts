﻿import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { DetailItemPage } from '../itemDetailPage/item';
//import { KeySearchPage } from '../keySearchPage/keySearchPage'
import { KeyPage } from "../key/key";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    constructor(public navCtrl: NavController,public event:Events) {
    }
    openFamilySearch() {
        this.navCtrl.push(DetailItemPage, { param: 'familylist', searchtype: 'family'});
    }
    openGenusSearch() {
        this.navCtrl.push(DetailItemPage, { param: 'genuslist', searchtype: 'genus'});
    }
    openPlantSearch() {
        this.navCtrl.push(DetailItemPage, { param: 'allist', searchtype:'all' });
    }
    openIdentificationKey() {
        this.navCtrl.push(KeyPage);
    }
}
