export class plantInformation {
    public dsc: string[];
    private url: string;
    public family_name: string;
    public subfamily_name: string;
    public genus_name: string;
    public species_name: string;
    public subspecies_name: string;
    public plantName: string;
    //sub
    public subset: string[];
    // type=='family'  indicate current page is on Family(the page is going to show a list of family ) etc. for genus,species,subspecies.
    public type: string;
    constructor() {
        this.subset = [];
    }
    public getDsc() {
        return this.dsc;
    }
    public setDsc(value: string[]) {
        this.dsc = value;
    }
    public getUrl() {
        return this.url;
    }
    public setUrl(value: string) {
        this.url = value;
    }
    public setFamily_name(value: string) {
        this.family_name = value;
    }
    public getFamily_name() {
        return this.family_name;
    }
    public getGenus_name(): string {
        return this.genus_name;
    }
    public setGenus_name(value: string) {
        this.genus_name = value;
    }
    public getSpecies_name(): string {
        return this.species_name;
    }
    public setSpecies_name(value: string) {
        this.species_name = value;
    }
    public getSubspecies_name(): string {
        return this.subspecies_name;
    }
    public setSubspecies_name(value: string) {
        this.subspecies_name = value;
    }
    public setType(value: string) {
        this.type = value;
    }
    public getType() {
        return this.type;
    }
    public setSubset(value: string[]){
        this.subset = value;
    }
    public getSubSet(){
        return this.subset;
    }
    public setPlName(val){
        this.plantName = val;
    }
    public getPlName(){
        return this.plantName;
    }
    public plStringToJSON(){
        return {'plantName':this.plantName,'family_name':this.family_name,'genus_name':this.genus_name,'species_name':this.species_name,'subspecies_name':this.subspecies_name,'type':this.type,'dsc':this.dsc};
    }
    public parse(raw){
    this.family_name = raw.family_name;
    this.genus_name = raw.genus_name;
    this.species_name = raw.species_name;
    this.subspecies_name = raw.subspecies_name;
    this.plantName = raw.plantName;
    this.type = raw.type;
    this.dsc = raw.dsc;
    }

}