﻿import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { ShareService } from '../../providers/shareService';
import { plantNetParser } from '../../providers/plantNetParser';
//import { Content } from 'ionic-angular';
import { plantInformation } from './PlantInformation'
import { AlertController } from 'ionic-angular';
import { LoadingController, Searchbar } from 'ionic-angular';


@Component({
    selector: 'item-page',
    templateUrl: 'item.html'
})
export class DetailItemPage {
    @ViewChild('mainSearchbar') searchBar: Searchbar;
    //todo
    //jscontainer is the place store a index json file for genus, p
    public jscontainer: any;

    //items is a arrary which contains element name that is used in 'item.html'
    public items: string[];
    //itemType is the variable to check current type of page.
    // itemType=='familylist'  indicate current page is on Family(the page is going to show a list of family ) etc. for genus,species,subspecies.
    // itemType=='endlist' indicate the page does not contains further sub pages.
    // itemType=='allist' indicate the current page that has combination of genus,species,subspecies.
    public itemType: any;
    //information for current page
    public plant: plantInformation = new plantInformation();
    //current_name is indicate the current selecting name by clicking on the button of a item in the list.
    public current_name: string = null;

    //  searchType is used to indicate which searching mode is in
    // it has three types of searching mode "family","genus","all" which corresponds to the first three search buttons in home.html
    public searchType: any;
    // this a flag for showing a decription section in the page 
    public showBox: Boolean = false;
    public showAddButton: boolean = true;
    public showFilter: Boolean = false;
    public filterFlag: Boolean = false;
    constructor(public navParams: NavParams, public navCtrl: NavController, private shareservice: ShareService, private parser: plantNetParser, private alertCtrl: AlertController, public loadingController: LoadingController, public events: Events) {
        this.initializeItems();
        let ccindex = this.shareservice.getCCdsc();
        if (ccindex[this.current_name] === undefined) {
            this.plant.setDsc(['undefined']);
            //console.log('current_name', this.current_name);
        }
        else {
            //console.log('current_name: ', this.current_name);
            this.plant.setDsc(ccindex[this.current_name]);
            //console.log('dsc length:', this.plant.getDsc().length);
        }
    }
    //set up essential information for page from previous page and direct the search function by checking searchType
    initializeItems() {
        this.itemType = this.navParams.get('param');
        this.searchType = this.navParams.get('searchtype');
        if (this.searchType == 'family') {
            this.searchByFamily();
        }
        if (this.searchType == 'genus') {
            this.searchByGenus();
        }
        if (this.searchType == 'all') {
            this.searchByAll();
        }
    }
    // only for windows
    // search bar focus when load
    ionViewDidEnter() {
        setTimeout(() => {
      this.searchBar.setFocus();
    }, 150);
    }   

    // ----------------------------------------------------------------------------
    // following code is used for initialise plant information for current page.
    //set up items for displaying plant information by a specific plant taxon
    display() {
        this.items = this.plant.subset;
    }

    //set up items for displaying all genus
    displayAllgenus() {
        this.items = Object.keys(this.shareservice.getGenusList()).sort();
    }
    //set up items for displaying all genus and all species and all subspecies
    dsiplayAll() {
        this.jscontainer = this.combineJs(this.shareservice.getSplist(), this.shareservice.getSubspList());
        let allist = Object.keys(this.jscontainer).concat(Object.keys(this.shareservice.getCommonNameIndex()));
        this.items = allist.sort();
    }
    //set up plant for current page
    setUpCurrentPlant() {
        if (this.itemType === 'subfamilylist' && this.searchType === 'family') {
            this.plant.setFamily_name(this.navParams.get('family_name'));
            this.current_name = this.plant.getFamily_name();
            this.plant.plantName = this.current_name;
        }
        if (this.itemType == 'genuslist' && this.searchType != 'genus') {
            this.plant.setType('family');
            this.plant.setFamily_name(this.navParams.get('family_name'));
            this.current_name = this.plant.getFamily_name();
            this.plant.plantName = this.current_name;
        }
        if (this.itemType == 'specieslist') {
            this.plant.setType('genus');
            this.plant.setFamily_name(this.navParams.get('family_name'));
            this.plant.setGenus_name(this.navParams.get('genus_name'));
            this.current_name = this.plant.getGenus_name();
            this.plant.plantName = this.current_name;
        }
        if (this.itemType == 'subspecieslist') {
            this.plant.setType('species');
            this.plant.setFamily_name(this.navParams.get('family_name'));
            this.plant.setGenus_name(this.navParams.get('genus_name'));
            this.plant.setSpecies_name(this.navParams.get('species_name'));
            this.current_name = this.plant.getGenus_name().concat(' ').concat(this.plant.getSpecies_name());
            this.plant.plantName = this.current_name;
        }
        if (this.itemType == 'endlist') {
            this.plant.setType('subspecies');
            this.plant.setFamily_name(this.navParams.get('family_name'));
            this.plant.setGenus_name(this.navParams.get('genus_name'));
            this.plant.setSpecies_name(this.navParams.get('species_name'));
            this.plant.setSubspecies_name(this.navParams.get('subspecies_name'))
            this.current_name = this.plant.getGenus_name().concat(' ').concat(this.plant.getSpecies_name()).concat(' ').concat(this.plant.getSubspecies_name());
            this.plant.plantName = this.current_name;
        }
    }
    // searching functionality for search by family
    searchByFamily() {
        if (this.itemType == 'familylist') {
            this.showBox = false;
            this.plant.setType('begin');
        } else if (this.itemType === 'subfamilylist') {
            this.showBox = true;
            this.plant.setType('subfamily');
            this.setUpCurrentPlant();
        }
        else {
            this.showBox = true;
            this.setUpCurrentPlant();
        }
        //set up sub list for current plant
        this.setSubset();

        this.display();
    }
    // searching functionality for search by genus
    searchByGenus() {
        if (this.itemType == 'genuslist') {
            this.showBox = false;
            this.displayAllgenus();
        }
        else {
            this.showBox = true;
            this.setUpCurrentPlant();
            this.setSubset();
            this.display();
        }

    }
    // searching functionality for search by genus/species
    searchByAll() {
        if (this.itemType == 'allist') {
            this.dsiplayAll();
            this.showFilter = true;
        }
        else {
            this.showBox = true;
            this.setUpCurrentPlant();
            this.setSubset();
            this.display();
        }

    }
    // end of partial functions for initialising plant information for current page.
    // ---------------------------------------------------------
    // the function that implements searching functionality for searchbar. 
    getItems(ev: any) {
        // Reset items back to all of the items
        if (this.filterFlag) {
            this.items = this.shareservice.getCClIdex().sort();
            console.log('getitem Items:', this.items.length);
        } else {
            this.initializeItems();
        }


        // set val to the value of the searchbar
        let val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter((item) => {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }

    }

    // navgation for clicking on each item of the list(items), it will navigate current page to a new item page
    openSubPage(item: string) {
        var virtualType = this.itemType;
        if (this.itemType == 'allist') {
            let commonNameIndex = this.shareservice.getCommonNameIndex();
            let check = commonNameIndex[item];
            if(check!==undefined){
               item = check; 
            }
            var target = this.jscontainer[item];
            this.plant.setFamily_name(target[0]);

            if (target.length == 4) {
                virtualType = 'subspecieslist';
                this.plant.setGenus_name(target[1]);
                this.plant.setSpecies_name(target[2]);
                this.plant.setSubspecies_name(target[3]);
            }
            if (target.length == 3) {
                virtualType = 'specieslist';
                this.plant.setGenus_name(target[1]);
                this.plant.setSpecies_name(target[2]);
            }
        }
        if (virtualType == 'familylist' || virtualType == 'subfamilylist') {
            this.plant.setFamily_name(item);
        }
        if (virtualType == 'genuslist') {
            this.plant.setGenus_name(item);
            if (this.searchType == 'genus') {
                this.plant.setFamily_name(this.shareservice.getGenusList()[this.plant.getGenus_name()][0]);
            }
        }
        if (virtualType == 'specieslist' && this.itemType != 'allist') {
            this.plant.setSpecies_name(item);
        }
        if (virtualType == 'subspecieslist' && this.itemType != 'allist') {
            this.plant.setSubspecies_name(item);
        }
        this.navCtrl.push(DetailItemPage, { param: this.getsubItemType(virtualType), searchtype: this.searchType, family_name: this.plant.getFamily_name(), genus_name: this.plant.getGenus_name(), species_name: this.plant.getSpecies_name(), subspecies_name: this.plant.getSubspecies_name() });
    }
    //function that returns a sub itemType for current page
    getsubItemType(itemType: any) {

        if (itemType === 'familylist' || itemType === 'subfamilylist') {
            if (this.hasSubFamily()) {
                return 'subfamilylist';
            } else {
                return 'genuslist';
            }

        }
        if (itemType == 'genuslist') {
            return 'specieslist';
        }
        if (itemType == 'specieslist') {
            return 'subspecieslist';
        }
        if (itemType == 'subspecieslist') {
            return 'endlist';
        }
    }
    public hasSubFamily(): boolean {
        //check by from json file
        let topIndex: {} = this.shareservice.getTopFamily();
        //console.log("has family", topIndex);
        //console.log("current Name:", this.plant.family_name);
        if (topIndex.hasOwnProperty(this.plant.getFamily_name())) {
            return true;
        } else {
            return false;
        }

    }
    //functions that returns a combining json object
    combineJs(obj1: JSON, obj2: JSON) {
        var result = {};
        for (var key in obj1) result[key] = obj1[key];
        for (var key in obj2) result[key] = obj2[key];
        return result;
    }


    filteringItems() {
        this.filterFlag = !this.filterFlag;
        //console.log("toggled: " + this.filterFlag);
        let event = new Promise((resolve, reject) => {
            resolve('Success!');
        });
        let loading = this.loadingController.create({
            spinner: 'crescent',
            content: 'Loading Please Wait...'
        });
        loading.present();

        if (this.filterFlag) {
            event.then((success) => {
                /*
                let ccindex = this.shareservice.getCCdsc();
                let prelength = this.items.length;
                console.log(ccindex);
                this.items = this.items.filter((item) => (ccindex[item] !== undefined)).sort();

                if (this.items.length != prelength) {
                    console.log('changed items:', this.items.length)
                }
                else {
                    console.log('no effect by filtering');
                }*/
                this.items = this.shareservice.getCClIdex().sort();
            }).then((value) => {
                setTimeout(() => {
                    loading.dismiss();
                }, 200);
            });
        }
        else {
            event.then((success) => {
                this.initializeItems();
            }).then((value) => {
                setTimeout(() => {
                    loading.dismiss();
                }, 500);
            });
        }
    }
    //get subset for current plant taxon
    public setSubset() {
        var myjsondata = this.shareservice.getJsonIndex();
        if (this.plant.type == 'begin') {
            let topIndex = this.shareservice.getTopFamily();
            let result = Object.keys(myjsondata).concat(Object.keys(topIndex));
            this.plant.subset = result.sort();
        }
        if (this.plant.type === 'subfamily') {
            let topIndex = this.shareservice.getTopFamily();
            this.plant.subset = topIndex[this.plant.getFamily_name()]["subfamily"];
            //read from subfamily json
        }
        if (this.plant.type == 'family') {
            let genusjson = myjsondata[this.plant.getFamily_name()]["genus"];
            this.plant.subset = Object.keys(genusjson).sort();
        }
        if (this.plant.type == 'genus') {
            let speciesjson = myjsondata[this.plant.getFamily_name()]["genus"][this.plant.getGenus_name()]["species"];
            this.plant.subset = Object.keys(speciesjson).sort();
        }
        if (this.plant.type == 'species') {
            let subspeciesjson = myjsondata[this.plant.getFamily_name()]["genus"][this.plant.getGenus_name()]["species"][this.plant.getSpecies_name()]["variants"];
            this.plant.subset = Object.keys(subspeciesjson).sort();
            if (typeof subspeciesjson != 'undefined') {
                this.plant.subset = Object.keys(subspeciesjson).sort();
            }
            else {
                this.plant.subset = [];
            }
        }
        //if the species has subspecies then
        if (this.plant.type == 'subspecies') {
            this.plant.subset = [];
        }
    }


}
