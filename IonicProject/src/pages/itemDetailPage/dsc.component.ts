﻿import {
    Component, OnChanges, Input,
    trigger, state, animate, transition, style
} from '@angular/core';

@Component({
    selector: 'my-dsc',
    template: `
    <div [@visibilityChanged]="visibility" *ngIf="visibility == 'shown'">
     <ion-card>
       <ion-card-content>
       <ion-item>
        <ion-icon name="information" item-left large></ion-icon>
       </ion-item>
        <div *ngFor="let description of dsc">
        <h4>{{description}}</h4>
        <br>
        </div>
        </ion-card-content>
       </ion-card>
    </div>
  `,
    styles: [`
    div {
      padding: 5px;
    }
  `],
    animations: [
        trigger('visibilityChanged', [
            state('shown', style({ opacity: 1, })),
            state('hidden', style({ opacity: 0, })),
            state('void', style({ opacity: 0})),
            transition('* => *', animate('.5s'))
        ])
    ]
})
export class DscComponent implements OnChanges {
    @Input() isVisible: boolean = false;
    @Input() dsc: String[];
    visibility = 'hidden';
    ngOnChanges() {
        this.visibility = this.isVisible ? 'shown' : 'hidden';
    }
}