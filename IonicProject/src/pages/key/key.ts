﻿import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ShareService } from '../../providers/shareService';
import { plantInformation } from '../itemDetailPage/PlantInformation';
import { DetailItemPage } from "../itemDetailPage/item";
@Component({
    templateUrl: 'key.html',
    selector: 'page-key'
})
// From user input work way through questions (Q3,Q4,Q5,Q6) finally return  next group (same process as before but different questions), 
// ultimaltely return array of plants families or/and species
// groups key:
//         *
//       /   \
//      q1    q2 
//     / \    
//    Q3  Q4 
//   /  \
//  Q5   Q6
//
// group6 key:
//         Q6
//       /   \
//      Q7    Q8
//     / \    
//    Q9  Q10 
//         \
//     Descriptions 

export class KeyPage {
    questions: string[]; // ['question one?', 'question two?']
    //indexOfQuestion: number; // The user selection: if they select woody? then the index is 0
    group: number; // Corresponds to json group1,group2i,...,groupn
    children: any; // [object, object]
    keygroups: any; // JSON version of paper plant key ID, loads it from data                               //wont load json
    rootNode: any; // [[id, parentid, question, group, ?plants ,children: [object, object]],[...]]
    groupType: any;
    qwLists: QuestionWithPlants[];
    //funcUserInput: () => any;
    //functUserOutput: (str: string) => void;

    constructor(public nav: NavController, public parms: NavParams, public share: ShareService) {
        this.rootNode = this.parms.get('NodeByQuestions');
        this.groupType = this.parms.get('GroupType');
        switch (this.groupType) {
            case "continueWithinGroup": {
                console.log("this.openSubKeyWithJson(share, " + this.groupType + ", " + this.rootNode + ");");
                this.openSubKeyWithJson(share, "continueWithinGroup", this.rootNode);
                break;
            }
            case undefined: {
                console.log("this.openSubKeyWithJson(share, " + this.groupType + ", " + this.rootNode + ");");
                this.openSubKeyWithJson(share, undefined, this.rootNode);
                break;
            }
            default: {
                console.log("this.openSubKeyWithJson(share, " + this.groupType + ", " + this.rootNode + ");");
                this.openSubKeyWithJson(share, this.groupType, this.rootNode);
                break;
            }
        }
        this.setupInfo();
        //console.log('question with plants:',this.rootNode.length);
    }
    // Initialise information for each question of current rootnode and plants of each question.
    public setupInfo() {
        this.qwLists = [];
        for (let index = 0; index < this.rootNode.length; index++) {
            let tempqw = new QuestionWithPlants();
            tempqw.$question = this.rootNode[index].question
            if (this.hasPlantsByIndex(index)) {
                //todo
                for (let p of this.rootNode[index].plants) {
                    let plant = new plantInformation();
                    plant.setFamily_name(p["family"]);
                    plant.setGenus_name(p["genus"]);
                    tempqw.$plants.push(plant);
                }
            } else {
                tempqw.$plants = [];
            }
            this.qwLists.push(tempqw);
        }
    }
    //check whether there is plants for current rootnode
    public hasPlantsByIndex(index: number) {
        if (this.rootNode[index].plants === undefined || this.rootNode[index].plants === null) {
            return false;
        }
        else {
            return true;
        }
    }
    public hasPlantsByQuestion(question: string) {
        let index = this.findIndexOfQuestion(question);
        if (this.rootNode[index].plants === undefined || this.rootNode[index].plants === null) {
            return false;
        }
        else {
            return true;
        }
    }
    public isNull(object) {
        if (object === null) {
            return true;
        }
        else {
            return false;
        }
    }

    public openPlantPage(plant: plantInformation) {
        //condition ? valueIfTrue : valueIfFalse
        //param: 'familylist', searchtype: 'family'
        //param: 'genuslist', searchtype: 'family'
        let input_searchType = 'family';
        
        console.log("current plant is ", plant);
        let itemType = this.isNull(plant.getGenus_name()) ? 'genuslist' : 'specieslist';
        if(this.hasSubFamily(plant)){
            itemType = 'subfamilylist';
        }
        // checking typo
        if (this.isPlantTypo(plant)) {
            alert(plant.getFamily_name() +" "+ plant.getGenus_name() + " is not matched");
        } else {
            this.nav.push(DetailItemPage, { param: itemType, searchtype: input_searchType, family_name: plant.getFamily_name(), genus_name: plant.getGenus_name() });
        }
    }
        public hasSubFamily(plant: plantInformation): boolean {
        //check by from json file
        let topIndex: {} = this.share.getTopFamily();
        //console.log("has family", topIndex);
        //console.log("current Name:", this.plant.family_name);
        if (topIndex.hasOwnProperty(plant.getFamily_name())) {
            return true;
        } else {
            return false;
        }
    }
    public isPlantTypo(plant: plantInformation) {
       let topIndex: {} = this.share.getTopFamily();
       let indexGenus = this.share.getGenusList();
       let  familyIndex = this.share.getJsonIndex();
       let type = this.isNull(plant.getGenus_name()) ? 'family' : 'genus';
       let firstCondiction = (familyIndex[plant.getFamily_name()]===undefined && topIndex[plant.getFamily_name()]===undefined);
       if(type === 'family'){
           if(firstCondiction){
               return true;
           } 
       }
       if(type ==='genus'){
            if( firstCondiction || indexGenus[plant.getGenus_name()]===undefined){
                return true;
            }
       }
       return false;
    }
    
   
    //      Input:
    // Creates new page for either: 
    // 1. The rest of the questions in groups json
    // 2. The very first instance from one group (group1,...,group6) (e.g. loads groups then loads first two questions woody? non woody?)
    // The options 1,2 lead to 3,4 options
    // 3. The rest of the questions from one group (group1,...,group6) 
    //      Output:
    // Family and species of multiple plants 
    public open(question) {

        var ind = this.findIndexOfQuestion(question);
        if (this.rootNode[ind].group === null && this.rootNode[ind].plants === undefined) {   //<-- creates page for key: groups
            this.nav.push(KeyPage, { "NodeByQuestions": this.getCurrentNodeByQuestion(ind).children, 'GroupType': this.checkNextPageType(ind) });

        } else if (this.rootNode[ind].group !== null && this.rootNode[ind].plants === undefined) {//group ==1,2i,2ii,3,4,5,6 && .plants doesnt exist
            this.nav.push(KeyPage, { 'NodeByQuestions': null, 'GroupType': this.checkNextPageType(ind) });

        } else if (this.rootNode[ind].group === undefined && this.rootNode[ind].plants === null) {//<-- creates page for key: group1...group6
            this.nav.push(KeyPage, { "NodeByQuestions": this.getCurrentNodeByQuestion(ind).children, 'GroupType': this.checkNextPageType(ind) });

        } else if (this.rootNode[ind].group === undefined && this.rootNode[ind].plants !== null) {
            // Diplay descriptions of plants             
            //this.displayPlants(this.rootNode[ind].plants);
        } else {
            console.log("Error in open(event, question)");
            console.log("OR json files not created");
        }
    }


    //Check group property to determine whether continue down tree or open new group
    public checkNextPageType(ind: any) {
        var whichGroup = this.rootNode[ind].group;
        switch (whichGroup) {
            case null: {
                return "continueWithinGroup";
            }
            case 1: {
                return "group1";
            }
            case "2i": {
                return "group2i";
            }
            case "2ii": {
                return "group2ii";
            }
            case 3: {
                return "group3";
            }            
            case 4: {
                return "group4";
            }
            case 5: {
                return "group5";
            }
            case 6: {
                return "group6";
            }            
            default: {
                return "continueWithinGroup";
            }
            // change variable and case when the group is added
            //testing purpose and it is used to prevent errors. 
            //default: {
            //    return "testing";
           // }
        }
    }

    public openSubKeyWithJson(share: ShareService, groupType: any, currentNode: any) {
        var key = share.getGroups();
        //console.log(currentNode);
        //console.log(key)
        switch (groupType) {
            //testing purpose and it is used to prevent errors.
            case "testing": {
                this.keygroups = key.testing;// the first JSON id key 
                this.rootNode = this.keygroups;
                this.questions = this.rootQuestions(this.keygroups);
                break;
            }
            case undefined: {
                this.keygroups = key.groups;// the first JSON id key 
                this.rootNode = this.keygroups;
                this.questions = this.rootQuestions(this.keygroups);
                break;
            }
            case "group1": {
                this.keygroups = key.group1;
                this.rootNode = this.keygroups;
                this.questions = this.rootQuestions(this.keygroups);
                break;
            }
            case "group2i": {
                this.keygroups = key.group2i;
                this.rootNode = this.keygroups;
                this.questions = this.rootQuestions(this.keygroups);
                break;
            }
            case "group2ii": {
                this.keygroups = key.group2ii;
                this.rootNode = this.keygroups;
                this.questions = this.rootQuestions(this.keygroups);
                break;
            }
            case "group3": {
                this.keygroups = key.group3;
                this.rootNode = this.keygroups;
                this.questions = this.rootQuestions(this.keygroups);
                break;
            }
            case "group4": {
                this.keygroups = key.group4;
                this.rootNode = this.keygroups;
                this.questions = this.rootQuestions(this.keygroups);
                break;
            }
            case "group5": {
                this.keygroups = key.group5;
                this.rootNode = this.keygroups;
                this.questions = this.rootQuestions(this.keygroups);
                break;
            }
            case "group6": {
                this.keygroups = key.group6;
                this.rootNode = this.keygroups;
                this.questions = this.rootQuestions(this.keygroups);
                break;
            }
            case "continueWithinGroup": {

                this.rootNode = currentNode;
                this.questions = this.childrenNodeQuestions(currentNode);
                break;
            }
            default: {
                this.rootNode = currentNode;
                this.questions = this.childrenNodeQuestions(currentNode);
                break;
            }
        }
    }

    // ['question one?', 'question two?'] returns 0 or 1
    public findIndexOfQuestion(str: string) {
        return this.questions.indexOf(str);
    }

    // Gets the inputed node children's questions and returns as an array
    private childrenNodeQuestions(objArr: any) {
        return objArr.map(function (obj) { return obj.question; });
    }

    //  Prints questions from root
    //        *
    //       / \
    //      Q1  Q2   
    public rootQuestions(obj: any) {
        //console.log("running rootQuestions()");
        //console.log(obj);
        return obj.map(function (key) { return key.question; });
    }

    // Return group corresponding to groups in field guide to the native plants of Sydney    
    public getGroup() {
        var newNode = this.rootNode;
        if (newNode.group == null) {
            return null;
        }
        else
            return newNode.group;
    }

    public getCurrentNodeByQuestion(selection) {
        return this.rootNode[selection];
    }

    /*
    // Test input from user in chrome console
    private promptUser() {
        var p = prompt("Please enter selection: ");
        return p;
    }

    // Test output to console
    private userOutput(str:string[]) {
        console.log(str);
        this.questions = str;       
    }*/

}

export class QuestionWithPlants {
    private question: string;
    private plants: plantInformation[];
    constructor() {
        this.$plants = [];
        this.question = '';
    }
    public get $question(): string {
        return this.question;
    }

    public set $question(value: string) {
        this.question = value;
    }

    public get $plants(): plantInformation[] {
        return this.plants;
    }

    public set $plants(value: plantInformation[]) {
        this.plants = value;
    }

}