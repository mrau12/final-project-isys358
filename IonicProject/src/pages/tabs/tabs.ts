﻿import { Events } from 'ionic-angular';
import { Component } from '@angular/core';

import { ReportSationPage } from '../reportSationPage/reportSationPage';
import { HomePage } from '../home/home';
import { About } from '../about/about';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ReportSationPage;
  tab3Root = About;

  constructor(public event: Events) {
      this.tab1Root = HomePage;
      this.tab2Root = ReportSationPage;
      this.tab3Root = About;
  }

  searchEntryTapped() {
    this.event.publish('searchEntry');
  }

  reportEntryTapped() {
    this.event.publish('reportEntry');
  }
}
