import { plantInformation } from './../itemDetailPage/PlantInformation';

export class PlantItem {
	public pl: plantInformation;
	private flag: Boolean;
	private comments: string;
	public pid:number;
	constructor() {
		this.$flag = false;
		this.$comments = '';
		this.$pl = new plantInformation();
		this.pl.setType(' ');
		this.pl.setPlName(' ');
		this.pid = -1;
	}


	public get $pid():number {
		return this.pid;
	}

	public set $pid(value: number) {
		this.pid = value;
	}

	public get $flag(): Boolean {
		return this.flag;
	}

	public set $flag(value: Boolean) {
		this.flag = value;
	}

	public get $comments(): string {
		return this.comments;
	}

	public set $comments(value: string) {
		this.comments = value;
	}


	public get $pl(): plantInformation {
		return this.pl;
	}

	public set $pl(value: plantInformation) {
		this.pl = value;
	}
	public generateJson(): {} {
		return {'pid':this.$pid,'pl': this.$pl.plStringToJSON(),'flag': this.$flag, 'comments': this.$comments };
	}
	public setContent(plitem: PlantItem) {
		this.$flag = plitem.$flag;
		this.$comments = plitem.$comments;
		this.$pl = plitem.$pl;
		this.$pid =plitem.$pid;
	}
	public loadData(raw) {
		//let raw = JSON.parse(_raw);
		this.$flag = raw.flag;
		this.$comments = raw.comments;
		this.$pid = raw.pid;
		this.$pl.parse(raw.pl);
	}
}