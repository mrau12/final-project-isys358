import { DBservice } from '../../providers/DBService';
import { PlantItem } from './plantItem';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, FabContainer, LoadingController } from 'ionic-angular';
import { ShareService } from '../../providers/shareService';
import { Observable } from 'rxjs/Rx';
/**
 * Generated class for the MorePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
	selector: 'plantItem-page',
	templateUrl: 'planItemPage.html',
})
export class PlantItemPage {
	private autoSave;
	plantItem: PlantItem;
	reportID: number;
	pid: number;
	direction = ''
	constructor(public navCtrl: NavController, public alerCtrl: AlertController, public navParams: NavParams, public db: DBservice, public loading: LoadingController, public shareservice: ShareService) {
		this.plantItem = new PlantItem();
		this.reportID = this.navParams.get('reportID');
		this.pid = this.navParams.get('pid');
		let ccindex = this.shareservice.getCCdsc();
		if (ccindex[this.plantItem.pl.plantName] === undefined) {
			this.plantItem.pl.setDsc(['undefined']);
			//console.log('current_name', this.plantItem.pl.plantName);
		}
		else {
			//console.log('current_name: ', this.plantItem.pl.plantName);
			this.plantItem.pl.setDsc(ccindex[this.plantItem.pl.plantName]);
			//console.log('dsc length:', this.plant.getDsc().length);
		}
		//this.load();
	}
	ionViewDidLoad() {
		this.autoSave = Observable.interval(5000).subscribe(x => {
			// the number 5000 is on miliseconds so every second is going to have an iteration of what is inside this code.
			if (this.direction !== 'delete') {
				this.save().then(() => {
					console.log("autoSaved!");
				});
			}
		});
	}
	private stopAutoSave() {
		this.autoSave.unsubscribe();
	}
	ionViewWillEnter() {
		this.load().then(() => {
			//console.log('current PL:', this.plantItem.pl);
		});
	}
	ionViewWillLeave() {
		this.stopAutoSave();
		if (this.direction !== 'delete') {
			this.save().then(() => {
			});
		}
	}

	delPlant(fab: FabContainer) {
		/* no confirm for plant deleting*/
		/*
		let confirm = this.alerCtrl.create({
			title: 'Delete',
			message: 'Are you sure you want to remove this plant?',
			buttons: [{
				text: 'Yes',
				handler: () => {
					this.db.removePlantItem(this.pid, this.reportID).then(() => {
						this.direction = 'delete';
						this.navCtrl.pop().then(() => {
							this.direction = ''
						});
					});
				}
			}, {
				text: 'No',
				handler: () => {
					console.log('Cancel clicked');
					fab.close()
				}
			}
			]
		});
		confirm.present();*/
		this.db.removePlantItem(this.pid, this.reportID).then(() => {
			this.direction = 'delete';
			this.navCtrl.pop().then(() => {
				this.direction = ''
			});
		});

	}
	load() {
		//console.log('before load', this.plantItem.pl);
		return this.db.getPlantItem(this.pid, this.reportID).then((plantItem) => {
			this.plantItem.setContent(plantItem);
		});
	}
	save() {
		return this.db.savePlantItemToDB(this.plantItem, this.reportID);
	}
	savToDB() {
	}

}
