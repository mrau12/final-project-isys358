﻿import { Report } from './report';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, Content, FabContainer, Events } from 'ionic-angular';
import { DBservice } from './../../providers/DBService';
//import { ShareService } from '../../providers/shareService';
import { DetailItemPage } from '../itemDetailPage/item';
import { PlantItemPage } from '../plantItemPage/plantItemPage';
import { LoadingController } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import * as FileSaver from 'file-saver';
@Component({
	selector: 'page-report',
	templateUrl: 'reportPage.html'
})
export class ReportPage {

	@ViewChild(Content) content: Content;
	public reportType;
	//public CachesRe: Report //cached report when the report page is just entered.
	public report: Report;
	//class plantItem[] { plant:Plantinformation, flag}
	public direction = '';
	private autoSave;
	//public flagIndex ={};
	constructor(public navParams: NavParams, public navCtrl: NavController, public alerCtrl: AlertController, public db: DBservice, public events: Events, public loading: LoadingController) {
		this.report = new Report();
		this.reportType = this.navParams.get('reportType');
		this.report.$reportID = this.navParams.get('reportid');

	}
	/**
	 * auto save function
	 */

	ionViewDidEnter() {
		console.log('report data loaded !', this.report);
		this.autoSave = Observable.interval(5000).subscribe(x => {
			// the number 5000 is on miliseconds so every second is going to have an iteration of what is inside this code.
			if (this.direction !== 'delete') {
				this.save().then(() => {
					console.log("autoSaved!");
				});
			}
		});
	}
	private stopAutoSave() {
		this.autoSave.unsubscribe();
	}

	ionViewWillLeave() {
		this.stopAutoSave();
		if (this.direction !== 'delete') {
			this.save().then(() => {

			});
		}
	}


	ionViewCanLeave() {
		let check: any;
		if (this.direction !== 'delete') {
			check = new Promise((resolve, reject) => {

				this.save().then(() => {

					resolve();
				});
			});
		} else {
			check = true;
		}

		return check;
	}





	ionViewWillEnter() {
		this.load().then(() => {
			this.events.publish('selected:reportid', this.report.$reportID);
		});
	}


	load() {
		return new Promise((resolve, reject) => {
			if (this.reportType === 'saved') {
				this.db.getReportByID(this.report.$reportID).then((report) => {
					this.report.setContent(report);
					resolve();
				});
			} else {
				//this.shareservice.setCurrentReportExists(true);
				if (this.reportType === 'new') {
					this.db.createNewReport().then(() => {
						this.db.getCurrentReport().then((report) => {
							this.report.setContent(report);
							//once 'new' is created the report type become current/
							this.reportType = 'current';
							resolve();
						});
					});
				} else {
					this.db.getCurrentReport().then((report) => {
						this.report.setContent(report);
						resolve();
					});
				}
			}
		}).then(() => {
			this.db.getPlantItemList(this.report.$reportID).then((js) => {
				this.report.setPlantItems(js);
			}).catch(() => {
				console.log('no plitem List!');
			});
		});

	}
	save() {
		let stack = [];
		if (this.report.$plantItems.length !== 0) {
			stack.push(this.db.savePlantItemListToDB(this.report.$plantItems, this.report.$reportID));
		}
		stack.push(this.db.saveReportToDBAsync(this.report, this.reportType));
		return Promise.all(stack);
	}
	// it will save current or a new report to be saved.
	saveToDB() {
		let loading = this.loading.create({
			spinner: 'crescent',
			content: 'Saving, Please Wait...'
		});
		loading.present();
		if (this.reportType === 'current') {
			this.db.CurrentTobeSaved().then(() => {
				this.reportType = 'saved';
				loading.dismiss();
			});
		}
		else {
			this.save().then(() => {
				loading.dismiss();
			});
		}
		loading.onDidDismiss(() => {
			console.log('current tpye: ', this.reportType)
		});

	}


	// delete report button
	delReport(fab: FabContainer) {
		let confirm = this.alerCtrl.create({
			title: 'Delete',
			message: 'Are you sure you want to delete this report?',
			buttons: [{
				text: 'Yes',
				handler: () => {
					this.db.removeReport(this.report.$reportID).then(() => {
						this.direction = 'delete';
						this.navCtrl.pop().then(() => {
							this.direction = '';
						});
					});
				}
			}, {
				text: 'No',
				handler: () => {
					console.log('Cancel clicked');
					fab.close();
				}
			}
			]
		});
		confirm.present();
	}

	SaveFileToLocal() {
		let contentType = 'text/csv';
		let strlist = [
			'"plantNo.","Plant Name","Notes"'
		]
		for(let item of this.report.$plantItems){
			let id = item.$pid;
			let name = item.pl.getPlName();
			let notes = item.$comments;
			let str = id+','+name+','+notes;
			strlist.push(str);
		}
		let CSV =strlist.join('\n');
		let data = new Blob([CSV], { type: contentType });
		let file_name = this.report.$reportTitle+'.csv';
		FileSaver.saveAs(data, file_name);
	}

	// add button
	openPlantSearch() {
		this.direction = 'push';
		this.navCtrl.push(DetailItemPage, { param: 'allist', searchtype: 'all' }).then((value) => {
			this.direction = '';
		});
	}

	// plant item Page
	viewPlanItem(pid_: number) {
		console.log('this pid is', pid_);
		this.direction = 'push';
		this.navCtrl.push(PlantItemPage, { reportID: this.report.$reportID, pid: pid_ }).then((value) => {
			this.direction = '';
		});;
	}

	scrollToTop(fab: FabContainer) {
		this.content.scrollToTop();
		fab.close();
	}

}
