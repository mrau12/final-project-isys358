import { PlantItem } from './../plantItemPage/plantItem';

export class Report {
	private reportTitle: string;
	private reportID: number;
	private author: string;
	private location: string;
	private date: string;
	private plantItems: PlantItem[];
	private comments: string;
	private plantItemsJson: {}[];
	public indexLength:number;
	constructor() {
		this.$author = '';
		this.$reportID = 0;
		this.$reportTitle = '';
		this.$location = '';
		this.$date = '';
		this.$comments = '';
		this.$plantItems = [];
		this.$plantItemsJson = [];
	}

	public get $comments(): string {
		return this.comments;
	}

	public set $comments(value: string) {
		this.comments = value;
	}

	public get $plantItems(): PlantItem[] {
		return this.plantItems;
	}

	public set $plantItems(value: PlantItem[]) {
		this.plantItems = value;
	}
	public get $reportTitle(): string {
		return this.reportTitle;
	}

	public set $reportTitle(value: string) {
		this.reportTitle = value;
	}


	public get $author(): string {
		return this.author;
	}

	public set $author(value: string) {
		this.author = value;
	}

	public get $location(): string {
		return this.location;
	}

	public set $location(value: string) {
		this.location = value;
	}

	public get $date(): string {
		return this.date;
	}

	public set $date(value: string) {
		this.date = value;
	}

	public get $reportID(): number {
		return this.reportID;
	}

	public set $reportID(value: number) {
		this.reportID = value;
	}

	public get $plantItemsJson(): {}[] {
		return this.plantItemsJson;
	}

	public set $plantItemsJson(value: {}[]) {
		this.plantItemsJson = value;
	}
	//saving method
	public generateJSON(): {} {
		//this.$plantItemsJson = this.listToJSON(this.$plantItems);
		let temp = {
			'id': this.$reportID, 'title': this.$reportTitle, 'author': this.$author, 'date': this.$date,
			'location': this.$location, 'comments': this.$comments
		};
		return temp;
	}
	//loading method
	public loadData(raw: any) {
		this.$author = raw.author;
		this.$reportID = raw.id;
		this.$reportTitle = raw.title;
		this.$location = raw.location;
		this.$date = raw.date;
		this.$comments = raw.comments;
		//this.$plantItemsJson = raw.plants;
		//this.$plantItems = this.JSONtoList(this.$plantItemsJson);
		//this.index = this.$plantItems.length-1;
	}
	public setPlantItems(raw){
		this.$plantItems = this.JSONtoList(raw);
	}
	public setPlantItemsJSON(){
		this.$plantItemsJson = this.listToJSON(this.$plantItems);
	}

	public JSONtoList(pljl: {}[]) {
		let pllist:PlantItem[]=[];
		for (let plj of pljl) {
			let plItem = new PlantItem();
			plItem.loadData(plj);
			pllist.push(plItem);
		}
		return pllist;
	}
	public listToJSON(list: PlantItem[]) {
		let jsonlist:{}[]=[];
		for (let pl of list) {
			let js = pl.generateJson();
			jsonlist.push(js);
		}
		return jsonlist;
	}
	public setContent(re: Report) {
		this.$author = re.$author;
		this.$comments = re.$comments;
		this.$date = re.$date;
		this.$location = re.$location;
		this.$plantItems = re.$plantItems;
		this.$reportID = re.$reportID;
		this.$reportTitle = re.$reportTitle;
		this.$plantItemsJson = re.$plantItemsJson;
	}



}