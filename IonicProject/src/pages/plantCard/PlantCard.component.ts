﻿import {
    Component, OnChanges, Input,
    trigger, state, animate, transition, style
} from '@angular/core';
import { plantInformation } from '../itemDetailPage/PlantInformation';
import { ShareService } from '../../providers/shareService';
import { plantNetParser } from '../../providers/plantNetParser';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { DBservice } from "../../providers/DBService";
import { ToastController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
    selector: 'plant-card',
    templateUrl: 'PlantCard.html',
    /*template: `
    <ion-card *ngIf="showBox">
        <ion-card-header>
            <ion-item>
                <ion-label>
                    Current Plant:
                </ion-label>
                <div *ngIf="showAddButton" item-content>
                    <button ion-button outline item-end color='secondary' (click)="addToReport()">Add to report</button>
                </div>
            </ion-item>
        </ion-card-header>
        <ion-card-content>
            <ion-item>
                <ion-icon name="leaf" item-left large></ion-icon>
                <h3><b>Family:</b></h3>
                <h4> {{plant.family_name}}</h4>
            </ion-item>
            <div *ngIf= "plant.type == 'genus' || plant.type == 'species' || plant.type == 'subspecies'">
                <ion-item>
                    <ion-icon name="leaf" item-left large></ion-icon>
                    <h3><b>Genus:</b></h3>
                    <h4> {{plant.genus_name}}</h4>
                </ion-item>
            </div>
            <div *ngIf="plant.type == 'species' || plant.type == 'subspecies'">
                <ion-item>
                    <ion-icon name="leaf" item-left large></ion-icon>
                    <h3><b>species:</b></h3>
                    <h4>{{plant.species_name}}</h4>
                </ion-item>
            </div>
            <div *ngIf="plant.type == 'subspecies'">
                <ion-item>
                    <ion-icon name="leaf" item-left large></ion-icon>
                    <h3><b>subspecies:</b></h3>
                    <h4>{{plant.subspecies_name}}</h4>
                </ion-item>
            </div>
            <div text-left>
                <button small ion-button round color="light" (click)="showOrHideDsc()">{{DscIndicateText}}</button>
            </div>
        </ion-card-content>
    </ion-card>
    <div [@visibilityChanged]="visibility" *ngIf="visibility == 'shown'">
     <ion-card>
       <ion-card-content>
       <ion-item>
        <ion-icon name="information" item-left large></ion-icon>
       </ion-item>
        <div *ngFor="let description of plant.dsc">
        <h4>{{description}}</h4>
        <br>
        </div>
        </ion-card-content>
       </ion-card>
    </div>
  `,*/
    styles: [`
    div {
      padding: 5px;
    }
  `],
    animations: [
        trigger('visibilityChanged', [
            state('shown', style({ opacity: 1, })),
            state('hidden', style({ opacity: 0, })),
            state('void', style({ opacity: 0 })),
            transition('* => *', animate('.5s'))
        ])
    ]
})
// <my-dsc [isVisible]="isVisible" [dsc]="plant.dsc"></my-dsc>
export class PlantCard implements OnChanges {
    @Input() plant: plantInformation = new plantInformation();
    @Input() showBox: boolean = false;
    @Input() showAddButton: boolean = false;
    DscIndicateText: string;
    isVisible: boolean = false;
    visibility = 'hidden';
    constructor(public db: DBservice, public shareservice: ShareService, private parser: plantNetParser, public alertCtrl: AlertController, public loadingController: LoadingController, public toastCtrl: ToastController,private iab: InAppBrowser) {
        this.DscIndicateText = "Show Description";

    }
    ngOnChanges() {
    }
    replaceAll(str, replaceWhat, replaceTo) {
        let re = new RegExp(replaceWhat, 'g');
        str = str.replace(re, replaceTo);
        return str;
    }
    showOrHideDsc() {
        if (!this.isVisible && this.plant.getDsc()[0] === 'undefined') {
            this.autoSetupUrl();
            let url = "http://plantnet.rbgsyd.nsw.gov.au".concat(this.plant.getUrl());
            // loading spinner 
            let loading = this.loadingController.create({
                spinner: 'crescent',
                content: 'Loading Please Wait...'
            });
            loading.present();
            // getting description for current plant by using parser.
            let parser = this.parser.Parser(url);
            parser.then((response: Cheerio[]) => {
                return response;
            }).then((data) => {
                let dsc: string[] = [];
                for (let i of data) {
                    dsc.push(i.text());
                }
                if (dsc.length === 0) {
                    dsc.push('No information found!');
                }
                if (dsc[0].match('APNI*')) {
                    dsc[0] = this.replaceAll(dsc[0], 'APNI', ' ');
                    dsc[0] = this.replaceAll(dsc[0], '[\*]', '');
                }
                let index = dsc.length - 1;
                if (index > 2) {
                    let start = dsc[index].indexOf("AVH");
                    dsc[index] = dsc[index].substring(0, start - 1);
                    start = dsc[index].indexOf("Named after");
                    dsc[index] = dsc[index].substring(0, start - 1);
                    start = dsc[index].indexOf("Text by");
                    dsc[index] = dsc[index].substring(0, start - 1);
                    start = dsc[index].indexOf("APNI*");
                    dsc[index] = dsc[index].substring(0, start - 1);
                }
                this.plant.setDsc(dsc);
                this.DscIndicateText = "Hide Description";
                //console.log("output assigned:", this.plant.getDsc());
                this.ChangeVisible();//this.isVisible = !this.isVisible;
                loading.dismiss();
            }
                ).catch((reject) => {
                    console.log("Error fetching :", reject);
                    loading.dismiss();
                    this.presentAlert();
                });
        }
        else {
            this.ChangeVisible()//this.isVisible = !this.isVisible;
            if (this.isVisible === false) {
                this.DscIndicateText = "Show Description";
            } else {
                this.DscIndicateText = "Hide Description";
            }
        }
    }
    public ChangeVisible() {
        this.isVisible = !this.isVisible;
        this.visibility = this.isVisible ? 'shown' : 'hidden';
    }
    presentAlert() {
        let alert = this.alertCtrl.create({
            title: 'Notification',
            subTitle: 'Unable to get description, please click button again!',
            buttons: ['OK']
        });
        alert.present();
    }
    showPageOnPlantNet() {
        this.autoSetupUrl();
        let url = "http://plantnet.rbgsyd.nsw.gov.au".concat(this.plant.getUrl());
        const browser = this.iab.create(url);
        browser.show();
    }
    public autoSetupUrl() {
        var myjsondata = this.shareservice.getJsonIndex();
        var link = "";
        if (this.plant.type === 'subfamily') {
            let toplist = this.shareservice.getTopFamily()
            link = toplist[this.plant.getFamily_name()]["family-link"];
        }
        if (this.plant.type == 'family') {
            link = myjsondata[this.plant.getFamily_name()]["family-link"];
        }
        if (this.plant.type == 'genus') {
            link = myjsondata[this.plant.getFamily_name()]["genus"][this.plant.getGenus_name()]["genus-link"];
        }
        if (this.plant.type == 'species') {
            link = myjsondata[this.plant.getFamily_name()]["genus"][this.plant.getGenus_name()]["species"][this.plant.getSpecies_name()]["species-link"];
        }
        if (this.plant.type == 'subspecies') {
            let temp = myjsondata[this.plant.getFamily_name()]["genus"][this.plant.getGenus_name()]["species"][this.plant.getSpecies_name()]["variants"][this.plant.getSubspecies_name()]["var-link"];
            if (typeof temp != 'undefined') {
                link = temp;
            }
        }
        this.plant.setUrl(link);
    }
    addToReport() {
        let loading = this.loadingController.create({
            spinner: 'crescent',
            content: 'Adding Please Wait...'
        });
        loading.present();
        if (this.db.session === 'report') {
            this.db.savelantItemToSelected(this.plant, this.db.selectingReportID).then((resolve) => {
                console.log('added Item');
                loading.dismiss().then(() => {
                    let toast = this.toastCtrl.create({
                        message: 'Plant added to selecting report.',
                        duration: 1000,
                        showCloseButton: true,
                        position: "top",
                        closeButtonText: "Close",
                    })
                    toast.present();
                });
            }).catch((reject) => {
                alert('no targed id');
                loading.dismiss();
            });
        } else {
            this.db.savePlantItemToCurrent(this.plant).then((resolve) => {
                console.log('added Item');
                loading.dismiss().then(() => {
                    let toast = this.toastCtrl.create({
                        message: 'Plant added to current report!',
                        duration: 1000,
                        showCloseButton: true,
                        position: "top",
                        closeButtonText: "Close",
                    })
                    toast.present();
                });
            }).catch((reject) => {
                this.db.createNewReport().then(() => {
                    console.log('added Item');
                    this.db.savePlantItemToCurrent(this.plant).then((resolve) => {
                        loading.dismiss().then(() => {
                            let toast = this.toastCtrl.create({
                                message: 'No current report!, added to a new report!',
                                duration: 1000,
                                showCloseButton: true,
                                position: "top",
                                closeButtonText: "Close",
                            })
                            toast.present();
                        });
                    });
                });
            });
        }

        /*
        let alertcl = this.alertCtrl.create({
            title: 'Add to Current Report',
            message: 'Do you want to add this plant to current report?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Add',
                    handler: () => {
                        //todo adding report function here
                        let loading = this.loadingController.create({
                            spinner: 'crescent',
                            content: 'Adding Please Wait...'
                        });
                        loading.present();
                        this.db.createPlantItemToCurrent(this.plant).then((resolve) => {
                            console.log('added Item');
                            loading.dismiss().then(() => {
                                let toast = this.toastCtrl.create({
                                    message: 'Plant added to current report!',
                                    duration: 2000,
                                    showCloseButton: true,
                                    position: "bottom",
                                    closeButtonText: "Close",
                                })
                                toast.present();
                            });
                        }).catch((reject) => {
                            alert('no current report!');
                            loading.dismiss();
                        });

                    }
                }
            ]
        });
        alertcl.present();*/
    }
}