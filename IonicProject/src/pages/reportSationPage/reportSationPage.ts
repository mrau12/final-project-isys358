﻿import { DBservice } from './../../providers/DBService';
import { Report } from './../reportPage/report';
import { Component } from '@angular/core';
import { NavController, AlertController, Events } from 'ionic-angular';
import { ReportPage } from '../reportPage/reportPage';

//import { ShareService } from '../../providers/shareService';
//import { Storage } from '@ionic/storage';

@Component({
	selector: 'page-reportStation',
	templateUrl: 'reportSationPage.html'
})
export class ReportSationPage {
	public CReport:Report;
	public reportList: Report[]; //a list of report that is used to display on page.
	public reportList1: Report[];
	constructor(public navCtrl: NavController, public alerCtrl: AlertController, public db: DBservice, public event:Events) {
		this.init();
		this.CReport=new Report();
	}
	init() {
		this.reportList = [];
		this.reportList1 = [];
		//event caller, setup current session in DB to Report
		//
		/*
		let a;
		this.DBservice.getCurrentReportFlag().then((val)=>{
			a = val;
			console.log('current flag:  ',a);
		}).then(()=>{
			this.DBservice.setCurrentReportFlag(!a).then(()=>{
				this.DBservice.getCurrentReportFlag().then((g)=>{
					console.log('second flag:  ',g);
				});
			});
		});*/
	}
	ionViewWillEnter(){
		this.db.getReportList().then((list)=>{
			this.reportList=list;
			console.log('updated list is :',this.reportList);
		});
		if(this.db.currentReportFlag===true){
			this.db.getCurrentReport().then((report)=>{
				this.CReport = report;
			});
		}
		
	}

	/* passed
	testNewToSaved() {
		this.DBservice.saveReportToDBAsync(this.reportList[1], 'new').then(() => {
			this.DBservice.getCurrentReport().then((report) => {
			console.log('currentReport 1:', report);
			console.log('currentReport Flag 1:', this.DBservice.currentReportFlag);
		});
			this.DBservice.CurrentTobeSaved().then(() => {
				console.log('currentTosaved');
			}).catch((resj) => {
				console.log('Reject');
			});
		});

	}*

	/* passed
	 testSavedAndGetID(){
				this.DBservice.getCurrentReport().then((report) => {
			console.log('currentReport 2:', report);
			console.log('currentReport Flag 2:', this.DBservice.currentReportFlag);
		}).catch(() => {
			console.log('flag is conflicted!1');
		}).then(() => {
			this.DBservice.saveReportToDBAsync(this.reportList[0], 'saved').then(() => {
				this.DBservice.getReportByID(1).then((report) => {
					console.log('report id 1:', report);
				}).catch(() => {
					console.log('failed to get!')
				});
			});
		});
		}*/

	public createNewReport() {
		this.navCtrl.push(ReportPage, { reportType: 'new' });
	}
	//todo
  
	newReport() {
		if (this.db.currentReportFlag) {
			let confirm = this.alerCtrl.create({
			  title: 'Create New Report',
			  message: 'The current report is not saved, would you like to save the current report and create a new report?',
			  buttons: [ {
				  text: 'Save & Create New Report',
				  handler: () => {
					//save function here for current report
					this.db.CurrentTobeSaved().then(()=>{
							this.createNewReport();
					});					
				  }
				} , {
				  text: 'View Current Report',
				  handler: () => {
					this.navCtrl.push(ReportPage, {reportType: 'current'});
				  }
				} , {
				  text: 'Cancel',
				  handler: () => {
					console.log('Cancel clicked');
				  }
				}
			  ]
			});
			confirm.present();
		} else {
			this.createNewReport();
		}
    }
	
	currentReport() {
		if (this.db.currentReportFlag) {
			this.navCtrl.push(ReportPage, {reportType:'current'});
		} else {
			let confirm = this.alerCtrl.create({
			  title: 'Current Report',
			  message: 'There is no current report, would you like to create a new report?',
			  buttons: [ {
				  text: 'Cancel',
				  handler: () => {
					console.log('Cancel clicked');
				  }
				} , {
				  text: 'New Report',
				  handler: () => {
					this.createNewReport();
				  }
				}
			  ]
			});
			confirm.present()
		}
    }
	
		openReport(id:number) {
        this.navCtrl.push(ReportPage, { reportid : id, reportType: 'saved'});
    }
	
}
