import { NgModule } from '@angular/core';
import { About } from './about';

@NgModule({
  declarations: [
    About,
  ],
  exports: [
    About
  ]
})
export class AboutModule {}
