# README #

The json file is in IonicProject/src/assets/data/plant_nested_data.json
The globle indexing json schema for plant：
```
{
    "Acanthaceae": {
        "family-link": "/cgi-bin/NSWfl.pl?page=nswfl&lvl=fm&name=Acanthaceae",
        "family-name": "Acanthaceae",
        "genus": {
            "Avicennia": {
                "family-name": "Acanthaceae",
                "genus-link": "/cgi-bin/NSWfl.pl?page=nswfl&lvl=gn&name=Avicennia",
                "genus-name": "Avicennia",
                "species": {
                    "marina": {
                        "family-name": "Acanthaceae",
                        "genus-name": "Avicennia",
                        "species-link": "/cgi-bin/NSWfl.pl?page=nswfl&lvl=sp&name=Avicennia~marina",
                        "species-name": "marina",
                        "variants": {
                            "subsp. australasica": {
                                "family-name": "Acanthaceae",
                                "genus-name": "Avicennia",
                                "species-name": "marina",
                                "var-link": "/cgi-bin/NSWfl.pl?page=nswfl&lvl=in&name=Avicennia~marina~subsp.+australasica",
                                "variants-name": "subsp. australasica"
							}
						}	
                    }
                }
            }
		}
	}
}
```

## Keys 

A key is a decision tree made up of opposing questions to help the user to identify a plant. We're basing our key on the popular key found in 'Field guide to the native plants of Sydney.' Each node corresponds to a json file found in the key branch.

### Main key (GROUPS)

```               
 GROUPS : Helps us determine our next key  G1,...,G6 
	G1:  Tree key
	G2:  Shrub Key
		G2i: Easily identifiable Shrubs
		G2ii: Identifying Shrubs from flowers
	G3: Moncotyledon Key
	G4: Key for Dicotyledon with leaves arising mainly from the base 
	G5: Key for Dicotyledon that creep and root along stems
	G6: Remaining Dicotyledon & Moncotyledon 
		G6i: Easily identifiable remaining Dicotyledon & Moncotyledon
		G6ii: Idenfying remaining Dicotyledon & Moncotyledon from flowers
```
### Sub-key (G3)

The above children nodes (G1,G2,G2i,G2ii,G3,G3i,G3ii,G4,G5,G6,G6i,G6ii) represents a sub key. The following example key is of the Group 3.

```  
G3:
	A. Flowering parts tiny
		B. Stems hollow. Grasses
			POACEAE
		*B. Stems  not hollow 
			SEDGES
			RUSHES
	*A. Flowers conspicious or if small then not enclosed
		C. Flowers irregular:
			ORCHIDACEAE
		*C. Flowers regular:
			AGAVACEAE
			ERIOCAULACEAE
			IRIDACEAE
			XANTHORRHOEACEAE
			COMMELINACEAE
			HAEMODORACEAE
			LILIACEAE
			XYRIDACEAE	
```   

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact